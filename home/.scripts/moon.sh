#!/usr/bin/env bash

set -eu -o pipefail

curl wttr.in/Moon --silent | sed '/^$/d;$d'
