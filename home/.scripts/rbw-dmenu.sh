#!/bin/sh
set -euo pipefail

# Check if rbw is locked, unlock if necessary.
rbw unlocked >/dev/null 2>&1 || rbw unlock

query=${1:-password}
DMENU=${DMENU:-dmenu}

chosen_item=$(rbw list | $DMENU)

# Exit if user didn't select anything
[ "$chosen_item" = "" ] && exit 1

case "$query" in
code)
	rbw code "$chosen_item"
	;;
*)
	# Select chosen item from vault, return login.query
	rbw get "$chosen_item" --raw | jq --join-output ".data.$query" | nohup xclip -loop 1 -sel clip >/dev/null
	;;
esac
