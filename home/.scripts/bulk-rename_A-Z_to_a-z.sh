#!/usr/bin/env bash
# script to replace all upper case to lower case
for f in *; 
	do mv -T "$f" "$(echo $f | tr [A-Z] [a-z])"; 
done
