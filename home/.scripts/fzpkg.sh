#!/bin/sh

set -eu

die() {
	echo >&2 "$0: $*"
	exit 1
}
getkey() {
	local orig=$(stty -f /dev/tty -g)
	stty -f /dev/tty -icanon min 0 time 20		# 2 secs.
	key=$(dd if=/dev/tty bs=1 count=1 2>/dev/null)
	stty -f /dev/tty "$orig"
	echo "$key"
}

for C in fzf bat
do	command -v "$C" >/dev/null || die "$C: reqd. cmd. not found"
done

D="$HOME/pkgsrc"
cd "$D"		# || exit 1

# Only category/package considered
#
P=$(printf '%s\n' */*/Makefile | sed 's;/Makefile$;;')
[ "$P" = "*/*" ] && die "$D: not a pkgsrc base dir."

for P in $(echo "$P" | fzf --preview 'bat {}/DESCR' --preview-window=60%)
do	echo "====> Going to build & install: $P (q to QUIT)"
	[ "$(getkey)" = q ] && break
	make -C "$P" install clean
done
