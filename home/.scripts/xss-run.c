/**
 * Run a program when the X screensaver is *turned off*.
 *
 * (used to run intel_backlight(1) to set a default LCD
 * brightness after coming out of DPMS standby, suspend, etc.)
 *
 * Compile:
 * cc -Wall -O2 -I/usr/X11R7/include -s -o xss-run xss-run.c \
 * 	-L/usr/X11R7/lib -Wl,-rpath=/usr/X11R7/lib -lX11 -lXext -lXss
 *
 * Usage:
 * xss-run /usr/pkg/bin/intel_backlight 85 >/dev/null 2>&1 &
 */

#include <sys/types.h>
#include <sys/wait.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <X11/Xlib.h>
#include <X11/extensions/dpms.h>
#include <X11/extensions/scrnsaver.h>

static void
run(int argc, char* argv[])
{
	int status;

	switch (fork()) {
	case -1:
		err(EXIT_FAILURE, "fork() failed");
	case 0:
		setsid();
		execv(argv[1], argv + 1);
		err(EXIT_FAILURE, "execv() failed: %s", argv[1]);
	default:
		wait(&status);
		break;
	}
}

int
main(int argc, char* argv[])
{
	Display *disp;
	int s_ev, s_er;
	int rc = EXIT_FAILURE;

	if (argc < 3) {
		fprintf(stderr, "Usage: %s PROG ARGS...\n", *argv);
		exit(rc);
	}
	nice(19);
	disp = XOpenDisplay(NULL);
	if (disp == NULL)
		errx(rc, "couldn't open display: %s", getenv("DISPLAY"));
	if (!DPMSQueryExtension(disp, &s_ev, &s_er)) {
		warnx("no DPMS extension");
		goto exit;
	}	if (!DPMSCapable(disp)) {
		warnx("no DPMS capability");
		goto exit;
	}
	if (!XScreenSaverQueryExtension(disp, &s_ev, &s_er)) {
		warnx("no screensaver extension");
		goto exit;
	}
	XScreenSaverSelectInput(disp, DefaultRootWindow(disp), ScreenSaverNotifyMask);
	for (;;) {
		XEvent ev;
		if (XNextEvent(disp, &ev))
			goto exit;	/* connection to X gone */
		if (ev.type != s_ev)
			continue;	/* not ours */
		if (((XScreenSaverNotifyEvent* )&ev)->state == ScreenSaverOff) {
			BOOL state;
			CARD16 lvl;
			if (!DPMSInfo(disp, &lvl, &state)) {
				warnx("DPMSInfo() failed");
				goto exit;
			}
			if (lvl == DPMSModeOn /* && state == True */)
				run(argc, argv);
		}
	}
	rc = EXIT_SUCCESS;
exit:
	XCloseDisplay(disp);
	return rc;
}
