#!/bin/sh

set -eu

# Bourne-shell string quoting
# turns:
#    hello world   -> 'hello world'
#    hello 'world' -> 'hello '\''world'\''
#
q()
{
    awk "
BEGIN {
    printf(\"'\")
    if (ARGC != 2)
        exit 0
    s=ARGV[1]
    n=length(s)
    for (i=1; i <= n; i++) {
        c=substr(s, i, 1)
        if (c == \"'\")
            printf(\"'\\\\%c'\", c)
        else
            printf(\"%c\", c)
    }
    printf(\"'\")
}" "$1"
}

F=""
for A
do    F="$F $(q "$A")"
done

umask 077
EDITOR=$(q "${EDITOR:=vi}")
exec su -m root -c "$EDITOR $F"