#!/bin/sh -e
#
# portable lock screen script

#image=/tmp/screen_locked.png

# Take a screenshot
#grim $image		# Wayland
#scrot $image		# X11

# If your locker doesn't provide options for blur etc.
# use ImageMagick instead (will be slower)
#mogrify \
#	-noise 0x9 \
#	-blur 0x5 \
#	$image

# X11
#i3lock -i $image

# Wayland
#swaylock \
#	--fade-in 1 \
#	--grace 5 \
#	--no-unlock-indicator \
#	-i $image \
#	--effect-blur 3x2 \
#	--effect-pixelate 1

#rm $image

# X11 - Easy mode (no screenshot necessary)
# First install `i3lockr` from https://github.com/owenthewizard/i3lockr
i3lockr --blur 50 --no-unlock-indicator # -i /usr/share/void-artwork/void-transparent.png
