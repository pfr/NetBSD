#!/bin/sh

PKGSRC=$HOME/.pkgsrc
OSABI=$HOME/.pkgsrc/pkgtools/osabi
XLINKS=$HOME/.pkgsrc/pkgtools/x11-links
WIP=$HOME/.pkgsrc/wip

su root -c "

	# remove osabi & x11-links before upgrade
	pkg_delete osabi-NetBSD-9.3_STABLE x11-links-1.35

	# update pkgsrc
	cd $PKGSRC
	su $USER -c 'cvs update -dP'

	# update repositories
	pkgin update

	# upgrade packages
	pkgin -y full-upgrade

	# remove orphan packages and clean pkgin
	pkgin -y autoremove && pkgin clean

	# build & install osabi
	cd $OSABI
	su $USER -c 'make -s'
	make install clean distclean clean-depends

	# build & install x11-links
	cd $XLINKS
	su $USER -c 'make -s'
	make install clean distclean clean-depends

	# update pkgsrc/wip
	cd $WIP
	su $USER -c 'git pull -r'

"
