#!/bin/sh

RESOLUTION=1366x768

# Use this to also capture sound (not working!)
#ffmpeg -f pad -i <input_device> -ac 2 -thread_queue_size 128 -f x11grab -s $RESOLUTION -i :0.0 -vcodec libx264 -preset ultrafast -threads 0 -y -loglevel quiet output.mkv

ffmpeg -f oss -i /dev/audio -thread_queue_size 128 -f x11grab -r 30 -s $RESOLUTION -i :0.0 -vcodec libx264 -preset ultrafast -threads 0 -y -loglevel quiet output.mkv

ffmpeg -i output.mkv workflow-${USER}.webm

rm output.mkv
