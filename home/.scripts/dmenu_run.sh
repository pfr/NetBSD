#!/bin/sh
DMENU=${DMENU:-dmenu}
dmenu_path | $DMENU "$@" | ${SHELL:-"/bin/sh"} &
