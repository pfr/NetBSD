#!/bin/sh
#
# nuniq.sh: Remove duplicate lines, leaving only the last one.
# (based on histsort.awk from "GAWK: Effective AWK Programming")
exec awk '
{
        lines[NR] = $0
        if ($0 in array)
                delete lines[array[$0]]
        array[$0] = NR
}
END {
        for (i = 1; i <= NR; i++)
                if (i in lines)
                        print lines[i]
}' "$@"
