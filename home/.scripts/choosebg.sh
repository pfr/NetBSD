#!/bin/bash

# ---------------------------------------------------------
# Name:    choosebg
# Author:  Luke Smith
# Descr.:  Script for launching sxiv as a previewer
#          of the images in user's wallpaper folder.
#          The user's expected to have a keybinding
#          on sxiv to execute a 'set wallpaper'
#          command from sxiv.
# ---------------------------------------------------------

wallpapers_folder=~/Pictures/walls

find $wallpapers_folder -type f | shuf | sxiv -t -
