#!/bin/sh

set -eu -o pipefail

echo
read -p "Choose Source:

1) RetroBSD
2) TildeClub

> " action

clear

if [ "$action" = "1" ]; then
    slrn \
    -f ~/.config/slrn/jnewsrc2 \
    -i ~/.config/slrn/slrnrc2 \
    -h retrobsd.ddns.net
fi

if [ "$action" = "2" ]; then
    slrn \
    -f ~/.config/slrc/jnewsrc \
    -i ~/.config/slrn/slrnrc \
    -h news.tilde.club
fi
