#!/bin/sh

set -eu -o pipefail

curl -s http://wttr.in/-37.731665,144.995026 | awk 'NR==2,NR==7' | sed '/^$/d'
