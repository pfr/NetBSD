#!/bin/sh
#
# requires the xprop and netpbm packages to be installed

set -o pipefail

# set your preferred screenshot directory
DIR=$HOME/Pictures/dumps

# output file format
FILE="$(date +"frame.%d-%m-%Y.%H:%M").png"

# get window id
ID=$(xprop -root 32x '\t$0' _NET_ACTIVE_WINDOW | awk {'print $2'})

# shoot your shot
xwd -silent -id $ID | xwdtopnm | pnmtopng > $DIR/$FILE

# send notification
notify-send -t 5000 -a "Screenshot Saved" "~/Pictures/dumps"
