#!/bin/sh -e
#  anonradio - streams SDF aNONradio via mpg123(1);
#  retries automatically if stream drops

if [ -x "$(which mpg123)" ]; then
  trap 'printf "\n anonradio stream terminated... \n" && exit 0' 2
  clear
  while true; do
    clear
    printf '\n          /$$   /$$ /$$$$$$ /$$   /$$                       /$$/$$ \n'
    printf '         | $$$ | $$/$$__  $| $$$ | $$                      | $|__/ \n'
    printf '  /$$$$$$| $$$$| $| $$  \\ $| $$$$| $$ /$$$$$$ /$$$$$$  /$$$$$$$/$$ /$$$$$$ \n'
    printf ' |____  $| $$ $$ $| $$  | $| $$ $$ $$/$$__  $|____  $$/$$__  $| $$/$$__  $$ \n'
    printf '  /$$$$$$| $$  $$$| $$  | $| $$  $$$| $$  \\__//$$$$$$| $$  | $| $| $$  \\ $$ \n'
    printf ' /$$__  $| $$\\  $$| $$  | $| $$\\  $$| $$     /$$__  $| $$  | $| $| $$  | $$ \n'
    printf '|  $$$$$$| $$ \\  $|  $$$$$$| $$ \\  $| $$    |  $$$$$$|  $$$$$$| $|  $$$$$$/ \n'
    printf ' \\_______|__/  \\__/\\______/|__/  \\__|__/     \\_______/\\_______|__/\\______/ \n'
    printf '\n'
    printf ' Listening on http://anonradio.net:8000/ \n'
    printf '\n'

    # Start mpg123 in verbose mode, capture ICY-META lines, and continuously update stream titles
    mpg123 -v -@ http://anonradio.net:8000/anonradio 2>&1 | \
      awk -F"'" '/ICY-META/ { cmd=" date +%H:%M:%S"; cmd | getline d; print d,"|", $2; close(cmd) }'

    sleep 5
  done
else
  echo 'mpg123(1) not found - exiting...'
  exit 1
fi
exit 0

