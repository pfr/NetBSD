#!/usr/bin/env bash
langs=(eng ara chi_sim chi_tra deu ell fin heb hun jpn kor nld rus tur)
lang=$(printf '%s\n' "${langs[@]}" | dmenu "$@")
maim -us | tesseract --dpi 145 -l eng+${lang} - - | xsel -bi
