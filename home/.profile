# .profile

# set text editor
export EDITOR="vim"

# set the search path for programs.
PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R7/bin:/usr/pkg/bin
PATH=${PATH}:/usr/pkg/sbin:/usr/games:/usr/local/bin:/usr/local/sbin:$HOME/.cargo/bin:$HOME/.local/bin
export PATH

# Configure the shell to load .shrc at startup time
export ENV=$HOME/.shrc

# set xdg runtime directory
export XDG_RUNTIME_DIR=/tmp/${USER}-runtime
if [ ! -d "${XDG_RUNTIME_DIR}" ]; then
	$(mkdir -m 0700 ${XDG_RUNTIME_DIR})
	if [ "${?}" -gt "0" ]; then
		echo "Unable to create \${XDG_RUNTIME_DIR}"
			":= ${XDG_RUNTIME_DIR}"
	fi
fi

# ksh history
export HISTFILE=$HOME/.ksh_history
export HISTSIZE=10000

## GPU Kernel bug
# for firefox
export MOZ_ACCELERATED=0
export MOZ_WEBRENDER=0
# for all MesaGL (see: https://docs.mesa3d.org/envvars.html)
#export LIBGL_ALWAYS_SOFTWARE=true

# shfm opener
export SHFM_OPENER="$HOME/.open.sh"

# UTF-8 Locale setting
export LANG=en_AU.UTF-8
export LC_CTYPE="$LANG"
export LC_ALL=""

# gpg
GPG_TTY=$(tty)
export GPG_TTY

# browser
export BROWSER=/usr/pkg/bin/firefox

# fzf options
export FZF_DEFAULT_OPTS='--height 100% --no-separator --layout=reverse --ansi --preview-window=75%'
export FZF_DEFAULT_COMMAND='fdfind --type file --follow --hidden'

# dmenu variables
export DMENU='dmenu -i -fn CodeNewRomanNerdFont:style=Regular:pixelsize=17:antialias=true -nb #2d2d2d -nf #c0c5ce -sb #5fb3b3 -sf #000000'
